import ExShoeShopRedux from "./ExShoeShopRedux/ExShoeShopRedux";

function App() {
  return (
    <div>
      <ExShoeShopRedux />
    </div>
  );
}

export default App;
