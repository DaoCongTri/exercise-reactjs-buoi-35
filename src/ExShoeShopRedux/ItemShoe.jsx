import React, { Component } from "react";
import { connect } from "react-redux";
import { viewAddToCartAction, viewDetailAction } from "./redux/action/action";
class ItemShoe extends Component {
  render() {
    let { data } = this.props;
    let { name, price, image } = data;
    return (
      <div className="col-4 pt-4">
        <div className="card text-left">
          <img
            className="card-img"
            src={image}
            alt=""
            width={200}
            height={150}
          />
          <div className="card-body" style={{ height: 150 }}>
            <p>{name}</p>
            <p>{price} $</p>
          </div>
          <button
            className="btn btn-secondary"
            onClick={() => {
              this.props.handleChangeDetail(data);
            }}
          >
            View more
          </button>
          <button
            className="btn btn-success"
            onClick={() => {
              this.props.handleAddToCart(data);
            }}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
let mapDispatchToState = (dispatch) => {
  return {
    handleChangeDetail: (data) => {
      dispatch(viewDetailAction(data));
    },
    handleAddToCart: (data) => {
      dispatch(viewAddToCartAction(data));
    },
  };
};
export default connect(null, mapDispatchToState)(ItemShoe);
