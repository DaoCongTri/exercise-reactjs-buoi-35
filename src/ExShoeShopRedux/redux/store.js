import { createStore } from "redux";
import { rootReducer } from "./reducer/rootReuducer";
import { composeWithDevTools } from "@redux-devtools/extension";
export const store = createStore(rootReducer, composeWithDevTools());
