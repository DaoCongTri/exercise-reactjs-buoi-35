export let viewDetailAction = (data) => {
  return {
    type: "CHANGE_DETAIL",
    payload: data,
  };
};
export let viewAddToCartAction = (data) => {
  return {
    type: "ADDTOCART",
    payload: data,
  };
};
export let viewDeleteAction = (data) => {
  return {
    type: "DELETE",
    payload: data,
  };
};
export let viewChangeAmountAction = (data, option) => {
  return {
    type: "CHANGE_QUANTITY",
    payload: {
      data,
      option,
    },
  };
};
