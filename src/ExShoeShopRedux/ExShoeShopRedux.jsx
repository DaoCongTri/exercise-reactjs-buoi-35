import React, { Component } from 'react'
import List from './List';
import Detail from './Detail';
import Cart from "./Cart";
export default class ExShoeShopRedux extends Component {
  render() {
    return (
      <div className="container">
        <h1 className='text-center bg-dark text-white p-4'>Exercise Shoe Shop Use Redux</h1>
        <div className="row">
          <List/>
          <Cart />
        </div>
        <Detail />
      </div>
    );
  }
}
