import React, { Component } from "react";
import { connect } from "react-redux";
import { viewChangeAmountAction, viewDeleteAction } from "./redux/action/action";

class Cart extends Component {
  render() {
    let { cart } = this.props;
    return (
      <div className="col-6 p-5">
        <h1 className="text-center">Cart</h1>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Section</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              let { id, name, image, price, quantity } = item;
              return (
                <tr key={index}>
                  <td>
                    <img src={image} width={100} height={100} alt="" />
                  </td>
                  <td>{name}</td>
                  <td>{price}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleChangeQuantity(id, "DOWN");
                      }}
                    >
                      -
                    </button>
                    {quantity}
                    <button
                      onClick={() => {
                        this.props.handleChangeQuantity(id, "UP");
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td>{price * quantity}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        this.props.handleDelete(id);
                      }}
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => { 
  return{
    cart: state.shoeReducer.listCart,
  };
};
let mapDispatchToState = (dispatch) => { 
  return {
    handleDelete: (data) => {
      dispatch(viewDeleteAction(data));
    },
    handleChangeQuantity: (data, option) => {
      dispatch(viewChangeAmountAction(data, option))
    }
  }
 }
export default connect(mapStateToProps, mapDispatchToState)(Cart);